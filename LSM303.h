// adres acc
#define ACC_ADDR 0x1D

// rejestry odczytow acc

#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D

// rejestry ustawien
#define CTRL0 0x1F
#define CTRL1 0x20
#define CTRL2 0x21
#define CTRL3 0x22
#define CTRL4 0x23
#define CTRL5 0x24
#define CTRL6 0x25
#define CTRL7 0x26

// Who Am I
#define WHO_AM_I 0x0F

// termometr
#define TEMP_OUT_L 0x05
#define TEMP_OUT_H 0x06
