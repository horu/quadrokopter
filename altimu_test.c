#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<linux/i2c-dev.h>
#include<fcntl.h>
#include<unistd.h>
#include<math.h>
#include"LSM303.h"
#include"LPS25H.h"
//#include<altimu.h>

 int plik;

#define PI 3.14159265358979323846
#define RAD_TO_DEG 57.29578

void writeAccReg(uint8_t reg, uint8_t value);
void selectDevice(int file, int addr);
void readAcc(int *a);
void readBlock(uint8_t command, uint8_t size, uint8_t *data);
void i2c_open();

int main()
{
 
 int accRaw[3];
 double gain=1;
 double AccXAngle, AccYAngle;
 


 i2c_open();

 selectDevice(plik, ACC_ADDR);

/* int result = i2c_smbus_read_byte_data(plik, WHO_AM_I);
  if (result < 0)
	{
     printf("Nie udalo sie przeczytac whoami\n"); 
	}else{
	 printf("%d\n", result);
	}
*/
 
 writeAccReg(CTRL1, 0b01010111);
 writeAccReg(CTRL2, 0b00100000);

for(int i=0;i<1;i++)
{
 readAcc(accRaw);

 AccXAngle=(float)(atan2(accRaw[1], accRaw[2])+PI)*RAD_TO_DEG;
 AccYAngle=(float)(atan2(accRaw[2], accRaw[0])+PI)*RAD_TO_DEG;

//Change the rotation value of the accelerometer to -/+ 180
if (AccXAngle >180)
{
          AccXAngle -= (float)360.0;
}
         if (AccYAngle >180)
         AccYAngle -= (float)360.0;

 printf("X: %lf\nY: %lf\nZ: %lf\nX angle: %3.2lf\tY angle: %lf\n\n", accRaw[0]*gain, accRaw[1]*gain, accRaw[2]*gain, AccXAngle, AccYAngle);
sleep(1);
}

/*
writeAccReg(CTRL5, 0b10010000);

int Temp_LSB = i2c_smbus_read_byte_data(plik, TEMP_OUT_L);
 if(Temp_LSB<0) printf("Nie udalo sie przeczytac LSB");
int Temp_MSB = i2c_smbus_read_byte_data(plik, 0x06);
 if(Temp_MSB<0) printf("Nie udalo sie przeczytac MSB");

printf("Temp_LSB: %d\tTemp_MSB: %d\n", Temp_LSB, Temp_MSB);

int tmp = (int16_t)(Temp_MSB <<8 | Temp_LSB);

double Temp=(tmp/480.0);
printf("TMP: %d\n", tmp);
printf("Temperatura: %lf\n", Temp);
*/ 

 selectDevice(plik, BAR_ADDR);

 i2c_smbus_write_byte_data(plik, CTRL_REG1_B, 0b11000000);

 int BAR_XL=i2c_smbus_read_byte_data(plik, PRESS_OUT_XL);
 int BAR_L=i2c_smbus_read_byte_data(plik, PRESS_OUT_L);
 int BAR_H=i2c_smbus_read_byte_data(plik, PRESS_OUT_H);

 int PRESS_TMP= (int32_t)(BAR_H <<16| BAR_L << 8 | BAR_XL);
 
printf("BAR_XL: %x\tBAR_L: %x\tBAR_H: %x\n", BAR_XL, BAR_L, BAR_H);
 printf("PRESS_TMP=%x\nPRESS_TMP/4096=%lf\n", PRESS_TMP, PRESS_TMP/4096.0);

int TEMP_L=i2c_smbus_read_byte_data(plik, TEMP_OUT_L_B);
int TEMP_H=i2c_smbus_read_byte_data(plik, TEMP_OUT_H_B);

int TEMP_TMP=(int16_t)(TEMP_L | TEMP_H << 8);
printf("TEMP_TMP: %x\n", TEMP_TMP);
printf("Temp: %lf\n", 42.5+TEMP_TMP/480.0);

printf("command: %x\n", OUT_X_L_A);
  
}
//--------------------------------------------------
//			Funkcje
//--------------------------------------------------
void selectDevice(int file, int addr)
{
	if(ioctl(file, I2C_SLAVE, addr) < 0)
	 {
		printf("Nie udalo sie wybrac urzadzenia\n");
	 }
}

void writeAccReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, ACC_ADDR);
	
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu acc\n");
		exit(1);
	 }
}

void readAcc(int *a)
{
 uint8_t block[6];
 
 selectDevice(plik, ACC_ADDR);
 readBlock(0x80 | OUT_X_L_A, sizeof(block), block);

printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*a = (int16_t)(block[0] | block[1] << 8);
	*(a+1) = (int16_t)(block[2] | block[3] << 8);
	*(a+2) = (int16_t)(block[4] | block[5] << 8);
}

void readBlock(uint8_t command, uint8_t size, uint8_t *data)
{
 int result=i2c_smbus_read_i2c_block_data(plik, command, size, data);

	if(result != size)
	 {
		printf("Nie przeczytano bloku z i2c\n");
		exit(1);
	 }
}

void readBar(int *a)
{
 uint8_t block[3];
 selectDevice(plik, BAR_ADDR);
 readBlock(PRESS_OUT_XL, sizeof(block), block);

	*a = (int32_t)(block[0] | block[1] << 8 | block[2] << 16);
}

void i2c_open()
{
 char filename[20];
 
 sprintf(filename, "/dev/i2c-%d", 1);
 plik = open(filename, O_RDWR);
 if(plik<0) { printf("Nie mozna otworzyc i2c\n"); exit(1);}
}
