#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<unistd.h>
#include<math.h>

#include "funkcje.h"

#define PI 3.1415926535897932384626433832795028841971
#define RAD_TO_DEG 57.29577951308

#define PRESS_M 1013.25
#define PRESS_M2 1005.0
#define RCONST 8.3144621
#define GCONST 9.8105
#define UCONST 0.0289644

int main()
{
 int press, temp;
 int rawAcc[3], rawGyr[3], rawMag[3];
 double GAIN_G=0.00875, GAIN_A=0.732, GAIN_M=0.479;
 float accXnorm,accYnorm,pitch,roll,magXcomp,magYcomp;

// Otworzenie portu i2c
 i2c_open();
// Ustawienie rejestrow
 regSetup();
// Odczytanie rejestrow identyfikacyjnych
// WhoAmIall();

// Odczyt wartosci: cisnienia, temperatury (barometr), 
// akcelerometru, zyroskopu i magnetometru
 press=readBar();
 temp=readTemp();
 readAcc(rawAcc);
 readGyr(rawGyr);
 readMag(rawMag);
 double temperatura=42.5+temp/480.0;
 double pressure=press/4096.0;
// double ref_press=press/4096.0;

// Wypisanie wszystkiego po kolei,
// razem z przeliczonymi wartosciami ze wzgledu na czulosc (patrz datasheet)
 printf("Cisnienie: %lf\n", pressure);
 printf("Temperatura: %lf\n\n", temperatura);
 printf("Raw Acc data:\nX: %d\tY: %d\tZ: %d\n", rawAcc[0], rawAcc[1], rawAcc[2]);
 printf("Acc data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawAcc[0]*GAIN_A, rawAcc[1]*GAIN_A, rawAcc[2]*GAIN_A); 
 printf("Raw Gyr data:\nX: %d\tY: %d\tZ: %d\n", rawGyr[0], rawGyr[1], rawGyr[2]);
 printf("Gyr data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawGyr[0]*GAIN_G, rawGyr[1]*GAIN_G, rawGyr[2]*GAIN_G);
 printf("Raw Mag data:\nX: %d\tY: %d\tZ: %d\n", rawMag[0], rawMag[1], rawMag[2]);
 printf("Mag data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawMag[0]*GAIN_M, rawMag[1]*GAIN_M, rawMag[2]*GAIN_M);

double h = -RCONST*temperatura*log(pressure/PRESS_M)/(UCONST*GCONST);
/*
//printf("-%lf*%lf*log(%lf/%lf)/(%lf*%lf)=",RCONST, temperatura, pressure, PRESS_M, UCONST, GCONST);
printf("Wysokosc n.p.m: %lf\n", h);
h = (-RCONST*temperatura*log(pressure/PRESS_M2))/(UCONST*GCONST);
printf("Wysokosc n.p.m: %lf\n", h);
*/
/*
double hh, hhh=0.0;
int k;
while(1)
{
for(k=0;k<1000;k++)
{
pressure=readBar()/4096.0;
hh= (1-pow(pressure/ref_press, 0.190263))*44330.8;
hhh+=hh;
}
hhh/=k;
printf("%lf\n", hhh);
hhh=0.0;
//sleep(1);
}
*/
/*printf("%lf\t%lf\n", log(pressure/PRESS_M), log(pressure/PRESS_M2));

// Obliczenie i wypisanie odchylenia od kierunku N (kompas)
 float heading = 180 * atan2(rawMag[1], rawMag[0])/PI;
 if(heading < 0)
	heading += 360;
 printf("Heading: %lf\n", heading);
*/
//Na razie nie wazne - test
/*
//Normalize accelerometer raw values.
	                accXnorm = rawAcc[0]/sqrt(rawAcc[0] * rawAcc[0] + rawAcc[1] * rawAcc[1] + rawAcc[2] * rawAcc[2]);
	                accYnorm = rawAcc[1]/sqrt(rawAcc[0] * rawAcc[0] + rawAcc[1] * rawAcc[1] + rawAcc[2] * rawAcc[2]);
	
			//Calculate pitch and roll
			pitch = asin(accXnorm);
			roll = -asin(accYnorm/cos(pitch));
	
			//Calculate the new tilt compensated values
			magXcomp = rawMag[0]*cos(pitch)+rawMag[02]*sin(pitch);
			magYcomp = rawMag[0]*sin(roll)*sin(pitch)+rawMag[1]*cos(roll)-rawMag[2]*sin(roll)*cos(pitch);
	
	
			//Calculate heading
			heading = 180*atan2(magYcomp,magXcomp)/PI;
	
	                //Convert heading to 0 - 360
			if(heading < 0)
			      heading += 360;
	
	
			printf("Compensated  Heading %7.3f  \n", heading);
*/
return 0 ;
}
