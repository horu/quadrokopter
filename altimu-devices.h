/*
		Deklaracje rejestrow dla Altimu-10_v4
*/

//---------------------------------------------------
//	Who Am I
// dla wszystkich taki sam
#define WHO_AM_I 0x0F

// Akcelerometr + magnetometr
// 	LSM303D

//	Adres
#define ACC_ADDR 0x1D

//	Who Am I
//#define WHO_AM_I_A 0x0F

//	Rejestry konfiguracyjne
#define CTRL0_A 0x1F
#define CTRL1_A 0x20
#define CTRL2_A 0x21
#define CTRL3_A 0x22
#define CTRL4_A 0x23
#define CTRL5_A 0x24
#define CTRL6_A 0x25
#define CTRL7_A 0x26

//	Rejestry wyjsciowe

// Termometr
#define TEMP_OUT_L_A 0x05
#define TEMP_OUT_H_A 0x06

// Magnetometr
#define OUT_X_L_M 0x08
#define OUT_X_H_M 0x09
#define OUT_Y_L_M 0x0A
#define OUT_Y_H_M 0x0B
#define OUT_Z_L_M 0x0C
#define OUT_Z_H_M 0x0D

// Akcelerometr
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D

//---------------------------------------------------
// Barometr
// LPS25H

//	Adres
#define BAR_ADDR 0x5D

//	Who Am I
//#define WHO_AM_I_P 0x0F

//	Rejestry konfiguracyjne
#define CTRL1_P 0x20
#define CTRL2_P 0x21
#define CTRL3_P 0x22
#define CTRL4_P 0x23

//	Rejestry wyjsciowe

// Termometr
#define TEMP_OUT_L_P 0x2B
#define TEMP_OUT_H_P 0x2C

// Barometr
#define PRESS_OUT_XL 0x28
#define PRESS_OUT_L 0x29
#define PRESS_OUT_H 0x2A

//---------------------------------------------------
// Zyroskop
// L3GD20H

//	Adres
#define GYR_ADDR 0x6B

//	Who Am I
//#define WHO_AM_I_G 0x0F

//	Rejestry konfiguracyjne
#define CTRL1_G 0x20
#define CTRL2_G 0x21
#define CTRL3_G 0x22
#define CTRL4_G 0x23
#define CTRL5_G 0x24

//	Rejestry wyjsciowe

// Termometr
#define OUT_TEMP_G 0x26

// Zyroskop
#define OUT_X_L_G 0x28
#define OUt_X_H_G 0x29
#define OUT_Y_L_G 0x2A
#define OUt_Y_H_G 0x2B
#define OUT_Z_L_G 0x2C
#define OUt_Z_H_G 0x2D

//---------------------------------------------------
// Sterownik serw (PWM)
// --------------------------------------------------

#define PI 3.1415926535897932384626433832795028841971
#define RAD_TO_DEG 57.29577951308
