// adres barometru
#define BAR_ADDR 0x5D

// WHO AM I
#define WHO_AM_I_B 0x0F

// rejestry kontrolujace
#define CTRL_REG1_B 0x20
#define CTRL_REG2_B 0x21
#define CTRL_REG3_B 0x22
#define CTRL_REG4_B 0x23

// rejestry z danymi
#define PRESS_OUT_XL 0x28
#define PRESS_OUT_L 0x29
#define PRESS_OUT_H 0x2A
#define TEMP_OUT_L_B 0x2B
#define TEMP_OUT_H_B 0x2C
