#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<stdint.h>
#include<linux/i2c-dev.h>

//#include "funkcje.h"
#include "altimu-devices.h"

int plik;

// Otworzenie wybranego portu i2c
// plik - zdefiniowany globalnie jako int
void i2c_open()
{
 char filename[20];
 
 sprintf(filename, "/dev/i2c-%d", 1);
 plik = open(filename, O_RDWR);
 if(plik<0) { printf("Nie mozna otworzyc i2c\n"); exit(1);}
}

// Wybor odpowiedniego urzadzenia na podstawie adresu (datasheet lub i2cdetect)
void selectDevice(int file, int addr)
{
	if(ioctl(file, I2C_SLAVE, addr) < 0)
	 {
		printf("Nie udalo sie wybrac urzadzenia\n");
	 }
}

// Pomocnicza funkcja pozwalajaca odczytac wartosci kilku rejestrow na raz
// command - nr rejestru z najwyzszym bitem 1, size - rozmiar tablicy,
// do ktorej zapisywane sa dane wyjsciowe, *data - tablica na dane
void readBlock(uint8_t command, uint8_t size, uint8_t *data)
{
 int result=i2c_smbus_read_i2c_block_data(plik, command, size, data);

	if(result != size)
	 {
		printf("Nie przeczytano bloku z i2c\n");
		exit(1);
	 }
}

// Zapis wartosci do wskazanego rejestru akcelerometru
// reg - adres rejestru, value - wartosc w formacie 0bxxxxxxxx (datasheet)
void writeAccReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, ACC_ADDR);
	
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu acc\n");
		exit(1);
	 }
}

// Odczyt wartosci z rejestrow wyjsciowych akcelerometru
// Odczytywane sa wszystkie osie na raz
void readAcc(int *a)
{
 uint8_t block[6];
 
 selectDevice(plik, ACC_ADDR);
 readBlock(0x80 | OUT_X_L_A, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*a = (int16_t)(block[0] | block[1] << 8);
	*(a+1) = (int16_t)(block[2] | block[3] << 8);
	*(a+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt z barometru
// Odczytywane sa wszystkie rejestry wyjsciowe i laczone w wynik
int readBar()
{
 uint8_t block[3];
 int p;
 selectDevice(plik, BAR_ADDR);
 readBlock(0x80 | PRESS_OUT_XL, sizeof(block), block);
//printf("%x %x %x\n", block[0], block[1], block[2]);
//printf("%x\n", block[0] | block[1]<<8 | block[2]<<16);

return p = (int32_t)(block[0] | block[1] << 8 | block[2] << 16);
}

// Odczyt temperatury z barometru
// Wszystko jak wyzej, tylko z innymi rejestrami
int readTemp()
{
 uint8_t block[2];
 int t;
	selectDevice(plik, BAR_ADDR);
	readBlock(0x80 | TEMP_OUT_L_P, sizeof(block), block);
return t = (int16_t)(block[0] | block[1] << 8);
}

// Zapis danych do wskaznego rejestru barometru
// Tak samo jak dla akcelerometru, tylko z adresem barometru
void writeBarReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, BAR_ADDR);
 
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu bar\n");
		exit(1);
	 }
}

// Zapis danych do wskaznego rejestru zyroskopu
// Tak samo jak dla akcelerometru, tylko z adresem zyroskopu
void writeGyrReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, GYR_ADDR);
	
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu gyr\n");
		exit(1);
	 }
}

// Odczyt temperatury z zyroskopu
// Jak dla barometru, tylko z innymi rejestrami
void readGyr(int *g)
{
 uint8_t block[6];
 
 selectDevice(plik, GYR_ADDR);
 readBlock(0x80 | OUT_X_L_G, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*g = (int16_t)(block[0] | block[1] << 8);
	*(g+1) = (int16_t)(block[2] | block[3] << 8);
	*(g+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt temperatury z magnetometru
// Jw.
void readMag(int *m)
{
 uint8_t block[6];
 
 selectDevice(plik, ACC_ADDR);
 readBlock(0x80 | OUT_X_L_M, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*m = (int16_t)(block[0] | block[1] << 8);
	*(m+1) = (int16_t)(block[2] | block[3] << 8);
	*(m+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt rejestru WHO_AM_I
// Adres rejestru taki sam dla kazdego urzadzenia
// Wystarczy podac adres urzadzenia - addr
// Podaje identyfikator w postaci liczy szesnastkowej i dziesietnej
void WhoAmI(int addr)
{
  selectDevice(plik, addr);

  int result = i2c_smbus_read_byte_data(plik, WHO_AM_I);
  if (result < 0)
	{
     printf("Nie udalo sie przeczytac whoami\n"); 
	}else{
	 printf("%x\t%d\n", result, result);
	}
}

// Minimalne ustawienia pozwalajace na normalna prace
void regSetup()
{
// Zapisanie odpowiednich wartości do rejestrów
// Ustawienie urzadzen (patrz datasheet)
// Zostanie wyrzucone do funkcji
 writeBarReg(CTRL1_P, 0b11000000);
 writeAccReg(CTRL1_A, 0b01010111);
 writeAccReg(CTRL2_A, 0b00100000);
 writeGyrReg(CTRL1_G, 0b10111111);
 writeAccReg(CTRL5_A, 0b11110000);
 writeAccReg(CTRL6_A, 0b01100000);
 writeAccReg(CTRL7_A, 0b00000000);
}

// Odczyt wszystkich rejestrów identyfikacyjnych
void WhoAmIall()
{
 WhoAmI(ACC_ADDR);
 WhoAmI(BAR_ADDR);
 WhoAmI(GYR_ADDR);
}

void test()
{
for(int i=0;i<5;i++)
printf("%d\n", i);
}
