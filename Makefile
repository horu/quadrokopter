CC=gcc
CFLAGS=-std=c99
LIBS=-lm

OBJS=altimu-10.o funkcje.o

default: altimu-10

altimu-10: $(OBJS)
	$(CC) $(LIBS) $(CFLAGS) $^ -o $@ 
altimu-10.o: altimu-10.c
	$(CC) $(CFLAGS) -c $^ -o $@ 
funkcje.o: funkcje.c
	$(CC) $(CFLAGS) -c $^ -o $@ 

clean:
	rm -f *.o
run:
	./altimu-10

.PHONY: clean run altimu-10
