#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<linux/i2c-dev.h>
#include<fcntl.h>
#include<unistd.h>
#include<math.h>

#include "altimu-devices.h"


void writeAccReg(uint8_t reg, uint8_t value);
void writeBarReg(uint8_t reg, uint8_t value);
void selectDevice(int file, int addr);
void readAcc(int *a);
void readBlock(uint8_t command, uint8_t size, uint8_t *data);
int readBar();
void i2c_open();
void WhoAmI(int addr);
int readTemp();
void writeGyrReg(uint8_t reg, uint8_t value);
void readGyr(int *p);
void readMag(int *m);

int plik;

int main()
{
 int pressure, temp;
 int rawAcc[3], rawGyr[3], rawMag[3];
 double GAIN_G=0.00875, GAIN_A=0.732, GAIN_M=0.479;
 float accXnorm,accYnorm,pitch,roll,magXcomp,magYcomp;

// Otworzenie portu i2c
 i2c_open();

// Odczyt identyfikatorow urzadzen - dla testu
 /*
 WhoAmI(ACC_ADDR);
 WhoAmI(BAR_ADDR);
 WhoAmI(GYR_ADDR);
*/

// Zapisanie odpowiednich wartości do rejestrów
// Ustawienie urzadzen (patrz datasheet)
// Zostanie wyrzucone do funkcji
 writeBarReg(CTRL1_P, 0b11000000);
 writeAccReg(CTRL1_A, 0b01010111);
 writeAccReg(CTRL2_A, 0b00100000);
 writeGyrReg(CTRL1_G, 0b10111111);
 writeAccReg(CTRL5_A, 0b11110000);
 writeAccReg(CTRL6_A, 0b01100000);
 writeAccReg(CTRL7_A, 0b00000000);


// Odczyt wartosci: cisnienia, temperatury (barometr), 
// akcelerometru, zyroskopu i magnetometru
 pressure=readBar();
 temp=readTemp();
 readAcc(rawAcc);
 readGyr(rawGyr);
 readMag(rawMag);
 

// Wypisanie wszystkiego po kolei,
// razem z przeliczonymi wartosciami ze wzgledu na czulosc (patrz datasheet)
 printf("Cisnienie: %lf\n", pressure/4096.0);
 printf("Temperatura: %lf\n\n", 42.5+temp/480.0);
 printf("Raw Acc data:\nX: %d\tY: %d\tZ: %d\n", rawAcc[0], rawAcc[1], rawAcc[2]);
 printf("Acc data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawAcc[0]*GAIN_A, rawAcc[1]*GAIN_A, rawAcc[2]*GAIN_A); 
 printf("Raw Gyr data:\nX: %d\tY: %d\tZ: %d\n", rawGyr[0], rawGyr[1], rawGyr[2]);
 printf("Gyr data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawGyr[0]*GAIN_G, rawGyr[1]*GAIN_G, rawGyr[2]*GAIN_G);
 printf("Raw Mag data:\nX: %d\tY: %d\tZ: %d\n", rawMag[0], rawMag[1], rawMag[2]);
 printf("Mag data:\nX: %lf\tY: %lf\tZ: %lf\n\n", rawMag[0]*GAIN_M, rawMag[1]*GAIN_M, rawMag[2]*GAIN_M);

// Obliczenie i wypisanie odchylenia od kierunku N (kompas)
 float heading = 180 * atan2(rawMag[1], rawMag[0])/PI;
 if(heading < 0)
	heading += 360;
 printf("Heading: %lf\n", heading);

//Na razie nie wazne - test
/*
//Normalize accelerometer raw values.
	                accXnorm = rawAcc[0]/sqrt(rawAcc[0] * rawAcc[0] + rawAcc[1] * rawAcc[1] + rawAcc[2] * rawAcc[2]);
	                accYnorm = rawAcc[1]/sqrt(rawAcc[0] * rawAcc[0] + rawAcc[1] * rawAcc[1] + rawAcc[2] * rawAcc[2]);
	
			//Calculate pitch and roll
			pitch = asin(accXnorm);
			roll = -asin(accYnorm/cos(pitch));
	
			//Calculate the new tilt compensated values
			magXcomp = rawMag[0]*cos(pitch)+rawMag[02]*sin(pitch);
			magYcomp = rawMag[0]*sin(roll)*sin(pitch)+rawMag[1]*cos(roll)-rawMag[2]*sin(roll)*cos(pitch);
	
	
			//Calculate heading
			heading = 180*atan2(magYcomp,magXcomp)/PI;
	
	                //Convert heading to 0 - 360
			if(heading < 0)
			      heading += 360;
	
	
			printf("Compensated  Heading %7.3f  \n", heading);
*/
return 0 ;
}

//--------------------------------------------------
//			Funkcje
//--------------------------------------------------

// Otworzenie wybranego portu i2c
// plik - zdefiniowany globalnie jako int
void i2c_open()
{
 char filename[20];
 
 sprintf(filename, "/dev/i2c-%d", 1);
 plik = open(filename, O_RDWR);
 if(plik<0) { printf("Nie mozna otworzyc i2c\n"); exit(1);}
}

// Wybor odpowiedniego urzadzenia na podstawie adresu (datasheet lub i2cdetect)
void selectDevice(int file, int addr)
{
	if(ioctl(file, I2C_SLAVE, addr) < 0)
	 {
		printf("Nie udalo sie wybrac urzadzenia\n");
	 }
}

// Pomocnicza funkcja pozwalajaca odczytac wartosci kilku rejestrow na raz
// command - nr rejestru z najwyzszym bitem 1, size - rozmiar tablicy,
// do ktorej zapisywane sa dane wyjsciowe, *data - tablica na dane
void readBlock(uint8_t command, uint8_t size, uint8_t *data)
{
 int result=i2c_smbus_read_i2c_block_data(plik, command, size, data);

	if(result != size)
	 {
		printf("Nie przeczytano bloku z i2c\n");
		exit(1);
	 }
}

// Zapis wartosci do wskazanego rejestru akcelerometru
// reg - adres rejestru, value - wartosc w formacie 0bxxxxxxxx (datasheet)
void writeAccReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, ACC_ADDR);
	
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu acc\n");
		exit(1);
	 }
}

// Odczyt wartosci z rejestrow wyjsciowych akcelerometru
// Odczytywane sa wszystkie osie na raz
void readAcc(int *a)
{
 uint8_t block[6];
 
 selectDevice(plik, ACC_ADDR);
 readBlock(0x80 | OUT_X_L_A, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*a = (int16_t)(block[0] | block[1] << 8);
	*(a+1) = (int16_t)(block[2] | block[3] << 8);
	*(a+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt z barometru
// Odczytywane sa wszystkie rejestry wyjsciowe i laczone w wynik
int readBar()
{
 uint8_t block[3];
 int p;
 selectDevice(plik, BAR_ADDR);
 readBlock(0x80 | PRESS_OUT_XL, sizeof(block), block);
//printf("%x %x %x\n", block[0], block[1], block[2]);
//printf("%x\n", block[0] | block[1]<<8 | block[2]<<16);

return p = (int32_t)(block[0] | block[1] << 8 | block[2] << 16);
}

// Odczyt temperatury z barometru
// Wszystko jak wyzej, tylko z innymi rejestrami
int readTemp()
{
 uint8_t block[2];
 int t;
	selectDevice(plik, BAR_ADDR);
	readBlock(0x80 | TEMP_OUT_L_P, sizeof(block), block);
return t = (int16_t)(block[0] | block[1] << 8);
}

// Zapis danych do wskaznego rejestru barometru
// Tak samo jak dla akcelerometru, tylko z adresem barometru
void writeBarReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, BAR_ADDR);
 
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu bar\n");
		exit(1);
	 }
}

// Zapis danych do wskaznego rejestru zyroskopu
// Tak samo jak dla akcelerometru, tylko z adresem zyroskopu
void writeGyrReg(uint8_t reg, uint8_t value)
{
 selectDevice(plik, GYR_ADDR);
	
 int result = i2c_smbus_write_byte_data(plik, reg, value);
	
	if(result == -1)
	 {
		printf("Nie zapisano do rejestu gyr\n");
		exit(1);
	 }
}

// Odczyt temperatury z zyroskopu
// Jak dla barometru, tylko z innymi rejestrami
void readGyr(int *g)
{
 uint8_t block[6];
 
 selectDevice(plik, GYR_ADDR);
 readBlock(0x80 | OUT_X_L_G, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*g = (int16_t)(block[0] | block[1] << 8);
	*(g+1) = (int16_t)(block[2] | block[3] << 8);
	*(g+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt temperatury z magnetometru
// Jw.
void readMag(int *m)
{
 uint8_t block[6];
 
 selectDevice(plik, ACC_ADDR);
 readBlock(0x80 | OUT_X_L_M, sizeof(block), block);

//printf("B1: %x, B2: %x\nB3: %x, B4: %x\nB5: %x, B6: %x\n", block[0], block[1], block[2], block[3], block[4], block[5]);
	*m = (int16_t)(block[0] | block[1] << 8);
	*(m+1) = (int16_t)(block[2] | block[3] << 8);
	*(m+2) = (int16_t)(block[4] | block[5] << 8);
}

// Odczyt rejestru WHO_AM_I
// Adres rejestru taki sam dla kazdego urzadzenia
// Wystarczy podac adres urzadzenia - addr
// Podaje identyfikator w postaci liczy szesnastkowej i dziesietnej
void WhoAmI(int addr)
{
  selectDevice(plik, addr);

  int result = i2c_smbus_read_byte_data(plik, WHO_AM_I);
  if (result < 0)
	{
     printf("Nie udalo sie przeczytac whoami\n"); 
	}else{
	 printf("%x\t%d\n", result, result);
	}
}
