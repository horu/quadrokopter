#ifndef FUNKCJE_H_INCLUDED
#define FUNKCJE_H_INCLUDED
extern int plik;
void writeAccReg(uint8_t reg, uint8_t value);
void writeBarReg(uint8_t reg, uint8_t value);
void selectDevice(int file, int addr);
void readAcc(int *a);
void readBlock(uint8_t command, uint8_t size, uint8_t *data);
int readBar();
void i2c_open();
void WhoAmI(int addr);
int readTemp();
void writeGyrReg(uint8_t reg, uint8_t value);
void readGyr(int *p);
void readMag(int *m);
void regSetup();
void WhoAmIall();
#endif
